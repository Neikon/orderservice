(ns orderservice.authorization
  (:require [clj-http.client :as client]))

(defn authorized? [user token]
  (prn "Authorizing " user " - " token)
  (let [authorization-response (client/post "http://localhost:3000/authorize" {:form-params  {"username"      user
                                                                                              "session-token" token}
                                                                               :content-type :json})]
    (prn ">>>>>>>>>>>>>> " authorization-response)
    true))