(ns orderservice.routes
  (:require [org.httpkit.server :as httpkit]
            [ring.util.http-response :as resp]
            [cheshire.core :refer :all]
            [orderservice.mongodb.mongo :as mongo]
            [orderservice.authorization :as auth]))

(defn serve-ping [_]
  (-> "pong"
      (resp/ok)
      (resp/content-type "text/html; charset=UTF-8")))

(defn serve-orders [{:keys [params] :as request}]
  (prn "Serving profile for request: " request)
  (let [user (:username params)
        session-token (:session-token params)
        orders (if (auth/authorized? user session-token)
                  (mongo/get-profile user)
                  (resp/unauthorized!))]
    (prn "Orders: ")
    (prn orders)
    (prn "/////////////////")
    (if orders
      (-> orders
          (resp/ok)
          (resp/content-type "text/html; charset=UTF-8")))))

(def routes ["/" {["ping/" :name] {:get serve-ping}
                  ["orders"]      {:get serve-orders}}])


